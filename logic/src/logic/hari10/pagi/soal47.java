package logic.hari10.pagi;

import java.util.Arrays;

public class soal47 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//	    terdapat int 3,4,2,6,8,7
//		carikan nilai terbesar dan nilai terkecilnya
//	    ex out:
//	    nilai terbesar=8
//	    nilai terkecil=2

		int[] a = { 3, 4, 2, 6, 8, 7 };

		// cara 1 tanpa logic
		Arrays.sort(a);
		System.out.println("nilai terbesar = "+a[a.length-1]);
		System.out.println("nilai terkecil = "+a[0]);
		
		//cara 2
		int besar=a[0];
		for (int i = 1; i < a.length; i++) {
			if(besar<a[i]) {
				besar=a[i];
			}
		}
		int kecil=a[0];
		for (int i = 1; i < a.length; i++) {
			if(kecil>a[i]) {
				kecil=a[i];
			}
		}
		 System.out.println("nilai terbesar = "+besar);
		 System.out.println("nilai terkecil = "+kecil);
		
	}

}
