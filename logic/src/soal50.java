import java.util.Scanner;

public class soal50 {

	public static void main(String[] args) {
//		di inputkan sebuah kata dan bilangan (1-10),
//		maka kata tadi akan di mundurkan sebanyak bilangan.
//		(hanya dari a sampai z).
//		ex1 :
//			in:
//				kata=abcd
//				bil=1
//			out:
//				katamundur=zabc
//		ex2 :
//			in:
//				kata=malam
//				bil=1
//			out:
//				katamundur=lzkzl
		
		Scanner read = new Scanner(System.in);
		System.out.print("kata = ");
		String a=read.nextLine();
		System.out.print("bil = ");
		int bil=read.nextInt();
		String mundur="";
		soal50 mndr = new soal50();
		mndr.cetak(a,bil);
		
	}
	public void cetak(String a,int bil) {
		String mundr = "";
		for(int i = 0;i < a.length();i++) {
			char ch = a.charAt(i);
			ch = (char)(ch-bil);
			if(ch<'a') {
				ch=(char)(ch-'a'+'z'+1);
			}
			mundr=mundr+ch;
		}
		System.out.println("kata mundur = "+mundr);
	}

}
